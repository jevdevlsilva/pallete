const navBtn = document.getElementById("menu-btn");
const navLinks = document.getElementById("nav-links");

navBtn.addEventListener("click", () => {
    if (navLinks.classList.contains("open")) {
        navLinks.classList.add("close");
        navLinks.addEventListener(
            "animationend",
            () => {
                navLinks.classList.remove("close");
                navLinks.classList.remove("open");
            },
            {once: true}
        );
    } else {
        navLinks.classList.add("open");
    }
});

navLinks.addEventListener("click", () => {
    navLinks.classList.add("close");
    navLinks.addEventListener(
        "animationend",
        () => {
            navLinks.classList.remove("close");
            navLinks.classList.remove("open");
        },
        {once: true}
    );
});

const services = document.querySelector(".services__wrapper-inner");

const servicesContent = Array.from(services.children);

servicesContent.forEach((item) => {
    const duplicateNode = item.cloneNode(true);
    duplicateNode.setAttribute("aria-hidden", true);
    services.appendChild(duplicateNode);
});

const swiper = new Swiper(".swiper", {
    slidesPerView: "auto",
    spaceBetween: 20,
});

const faq = document.querySelector(".faq__grid");

faq.addEventListener("click", (e) => {
    const target = e.target;
    const faqCard = target.closest(".faq__card");
    if (target.classList.contains("ri-arrow-down-s-line")) {
        if (faqCard.classList.contains("active")) {
            faqCard.classList.remove("active");
        } else {
            Array.from(faq.children).forEach((item) => {
                item.classList.remove("active");
            });
            faqCard.classList.add("active");
        }
    }
});

// const logo = document.querySelector('.logo-text');
//
// // Verifica se a largura da janela é maior que 768 pixels (desktop)
// if (window.innerWidth > 768) {
//   // Adiciona um ouvinte de evento de rolagem à janela
//   window.addEventListener('scroll', () => {
//     // Obtém a posição atual de rolagem da página
//     const scrollPosition = window.scrollY;
//
//     // Calcula o ângulo de rotação com base na posição de rolagem
//     const rotationAngle = scrollPosition *(0.1); // Ajuste o fator de rotação conforme necessário
//
//     // Aplica a transformação CSS para girar a logo
//     logo.style.transform = `rotate(${rotationAngle}deg)`;
//   });
// }
const ITEMS = document.querySelectorAll('.item')

ITEMS.forEach(item => {
    item.addEventListener('click', () => {
        removeActionClasses()
        item.classList.add('active')
    })
})

function removeActionClasses() {
    ITEMS.forEach(item => {
        item.classList.remove('active')
    })
}

window.addEventListener('scroll', function () {
    const socialIcons = document.querySelector('.social-icons');
    const scrollTop = window.pageYOffset || document.documentElement.scrollTop;

    if (scrollTop > 100) { // Defina a altura em que deseja que os ícones se movam
        socialIcons.classList.add('scrolling');
    } else {
        socialIcons.classList.remove('scrolling');
    }
});


var images = [
    'assets/portifolio-1.jpeg',
    'assets/p-2.jpeg',
    'assets/p-4.jpeg',
    'assets/p-5.jpeg',
    'assets/p-6.jpeg',
];
function preloadImages(images) {
    for (let i = 0; i < images.length; i++) {
        let img = new Image();
        img.src = images[i];
    }
}

preloadImages(images);

let currentIndex = 0;

function changeImage() {
    // document.getElementById('carouselImage').style.opacity = 0;

    setTimeout(function() {
        document.getElementById('carouselImage').src = images[currentIndex + 1];

        currentIndex++;

        if (currentIndex >= images.length) {
            currentIndex = 0;
            document.getElementById('carouselImage').src = images[0];
        }

        // document.getElementById('carouselImage').style.opacity = 1;
    }, 1000);
}

setInterval(changeImage, 4000);